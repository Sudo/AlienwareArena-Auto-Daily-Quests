# Contributing

The following is a set of guidelines for contributing to the project.

Pull requests via codeberg are the best way to contribute, and are always welcome. Feel free to suggest changes to this or any other document in a pull request, issue or directly communicating to me as well.

---

## Content

- [Guidelines](#guidelines)
- [Addding new features](#adding-new-features)
  - [Add quest completion](#add-quest-completion)
- [Testing](#testing)
- [Translating](#translating)

---

## Guidelines

The following are guidelines for making changes.

### Correct 👍

- Use in-build site features and exposed global variables/functions.
- Try to avoid, if possible, to rely on page elements.

### Incorrect 👎

- Bump version of the userscript in the pull request.
- Include a external library (pollyfills being an exception)
- Expose window variables without verification.
- DOM manipulation.
- Inline code injection.

### Style guide

Code formatting is mostly a matter of code legibility. Your code will probably work, but readable code is more valuable. Therefore, please stick to the Eslint and Prettier configuration of the project.

### Code comments guide

Comments helps other developers understand the code easily and bring code highlighting in supported IDEs. Please, keep with the JSDocs/Doxygen comment format for functions.

Is allowed to use as starting character any default tags of "Better Comments" extension.

### Git commit message convention

If you are using VSCode I recommend using [Commit Message Editor](https://github.com/bendera/vscode-commit-message-editor).

- Separate subject from body with a blank line
- Limit the subject line to 50 characters
- Capitalize the subject line
- Do not end the subject line with a period
- Use the imperative mood in the subject line
- Wrap the body at 72 characters
- Use the body to explain what and why vs. how

I left here a template I use:

```md
Short (max 50 chars) summary of changes (imperative)

Message body, explanation (wrap lines to about 72 characters or so)

Tags: {tags if using}
Resolves: {issue ID}
Fixes: {issue ID or description}
See also: {issue/PR ID}

Note:
```

## Adding new features

### Add quest completion

Sometimes it is needed to add another quest completion to the quest list.
(note that new types need to be added manually to the rest API and will not work straight away without this)

> New quest code example:

```js
const quests = [
  [...]
  // Example
  {
    type: 'example_quest',
    action: (quest_name) => {
      // will run when the type matches the current quest name
      // the function receive the quest name as a prop if needed.

      // When completed, call afterQuestCompletion function and pass the name
      AD.afterQuestCompletion(quest_name)
    }
  },
  [...]
]
```

## Testing

Testing must be performed in Firefox and any Chromium based browser.

Testing must be performed with Tampermonkey, ~~GreasyFork~~ and Violentmonkey.

## Translating

Translations are not planned nor intended to be a thing for a common userscript. But, you can help translating to your language the metadata if you want!
