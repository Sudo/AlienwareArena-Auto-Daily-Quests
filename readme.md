# AlienwareArena - Auto Daily-Quests

## Features

- Automatic daily quest completion
- Automatic daily event rewards redemption

## Installation

- **Google Chrome:** Install [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo) or [Violentmonkey](https://chrome.google.com/webstore/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag).
- **Mozilla Firefox:** Install [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/), [Tampermonkey](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/) or [Violentmonkey](https://addons.mozilla.org/en-US/firefox/addon/violentmonkey/).
- **[Click here](https://codeberg.org/Sudo/AlienwareArena-Auto-Daily-Quests/raw/branch/%27master%27/autoAWA.user.js)** to install through this repository or **[here](https://greasyfork.org/en/scripts/437670-alienwarearena-auto-daily-quests)** to install through GreasyFork (synced updates).
- Confirm your wish to install the Userscript.

## Usage

1. Install the userscript with your preferred manager
2. Go to `alienwarearena.com`
3. Log in
4. The quest should be performed automatically

Quick Notes:

- If any error occurs, check the console messages with the flag `config.verbose_logging: true`
- This script uses JQuery provided by the same page.
- If opted-in, a copy of the quest type and names will be saved locally.

## Known Issues

- Modifying the account in `update_about_me` action **was not tested** with a phone number (page reject mine). **Probably will remove the phone number from the account**. (you can opt-out setting `allow_modify_userAccount` to `false`)
- Userscript managers that use the User Scripts API provided by Firefox will not work.
- Some news can be under the `dell` subdomain, generating CORS to disallow the request. This news is quite rare enough to ignore (3 when writing this).
- The userscript is dependent on the site language, so it will not work in other languages than English (if this ever happens).
- Some quests are region-locked and may not be noticed by me, thus that quest may never be added if no one reports it :(

## Contributing

Feel free to share your improvements with a PR!

### Development

We use [Eslint](https://eslint.org) and [Prettier](https://prettier.io) to ensure code quality and style rules.

### Prerequisites

- [Git](https://git-scm.com)
- [Node.js](https://nodejs.org) with NPM

## Rest API

**[ If you're reading this before the formal release, note that reporting is not yet available and will always return an error. ]**

**Service provided by [restdb.io](https://restdb.io/)**

**Feel free to use it in other AWA related projects!**

Due to recent changes introduced with the new ARP6 update, the native status API has removed the `quests` object used by this userscript, and probably others, to "know" what actions need to perform automatically.

In response, I have created my own REST API to keep track of the quest types with their names.

**Notice**

- For security reasons the API is only allowed to respond to requests from the AWA domain.
- The API will be automatically rejected if the header misses the API key:
  - `x-apikey`: `625f4505fcf9897eb1119d6f`

### API Usage

Example `GET` request using XMLHttpRequest

```javascript
var data = null
var xhr = new XMLHttpRequest()
xhr.withCredentials = false
xhr.addEventListener('readystatechange', () => {
	if (this.readyState === 4) console.log(this.responseText)
})
xhr.open('GET', 'https://awaquest-a94f.restdb.io/rest/quests')
xhr.setRequestHeader('content-type', 'application/json')
xhr.setRequestHeader('x-apikey', '625f4505fcf9897eb1119d6f') // NOTICE THE API KEY!
xhr.send(data)
```

```javascript
/*
 * Response
*/
[
 {"_id":"625cdc1bc894c13800357a49","type":"update_about_me", "names":["What's new with you?", ...]},
  ...
]
```

### Reporting new quests

Because sharing is caring! Help to keep the API up to date with the new quests.

Reporting new quests is done by sending a `POST` request to the API, with the following JSON payload:

```javascript
quest: {
  type: "update_about_me" // Quest type, see the API response for the list of quest types
  name: "What's new with you?", // Name of the quest
  user_id: 0123457 // User ID - OPTIONAL
}
```

**IMPORTANT NOTES WHEN REPORTING NEW QUESTS:**

- **Quest `type` requires** to be a valid quest type (you can see the API response for the list of quest types).
- **New quest types reported** require to be manually added.
- **`user_id` is optional and opt-in**. If you include your user ID, after a few accepted reports, your ID will become whitelisted and automatically accepted without manual intervention.
- The request will be rejected if contains any of these banned characters: `@#$%^&*()+\-=[\]{};:"\\|<>/~`
- The request will be rejected if the type does not exist or the name already exists.

New quest report example using XMLHttpRequest:

```javascript
let data = JSON.stringify({
	quest: {
		type: 'read_articles', // QUEST TYPE
		name: 'Keep up with the times', // QUEST NAME
		user_id: 0123457, // USER ID - OPTIONAL
	},
})

let xhr = new XMLHttpRequest()
xhr.withCredentials = false
xhr.addEventListener('readystatechange', () => {
	if (this.readyState === 4) console.log(this.responseText)
})
xhr.open('POST', 'https://awaquest-a94f.restdb.io/rest/quests')
xhr.setRequestHeader('content-type', 'application/json')
xhr.setRequestHeader('x-apikey', '625f4505fcf9897eb1119d6f') // NOTICE THE API KEY
xhr.send(data) // WILL THROW A "ALREADY EXISTS" ERROR
```

### API Privacy Policy

If you reported a new quest including your User ID and you have been added to the whitelist, your ID will be stored in a private database hashed for your privacy. I will be not able to know who's who.

A third-party service is used to provide this API, you can read [restdb.io's privacy policy](https://restdb.io/privacy/) for more information.

### Request quests database copy

Even though is the same as using the API, it is possible to download the database in a CSV format.

If you want to request a copy of the database you can contact me through Matrix or email, provided in my profile!

---

**Disclaimer: Any usage of this script may violate the Terms of Service of the sites it runs on. Use it at your own risk.**
